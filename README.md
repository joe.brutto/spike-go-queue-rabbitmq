# Spike - Go+RabbitMQ

Testing integration of [Go](https://golang.org/) with [RabbitMQ](https://www.rabbitmq.com/). I
often use RabbitMQ for testing when I do not have [Apache Kafka](https://kafka.apache.org/) in
my production environment simply for local testing (if I am using Apache Kafka in production I
use that setup directly). RabbitMQ takes up less resources to execute locally and is simply
just another option I like to have available to me when I'm doing prep work, setup, etc. I do
not use it in production any more* (I did quite heavily at one point in time).

## Covered in the Project

This is what I have written samples for *so far*...

 * [Topics](https://www.rabbitmq.com/tutorials/tutorial-one-go.html)
 
## Getting Set Up

To get started, you really just need to run [Docker Compose](https://docs.docker.com/compose/):

    docker-compose up -d

That should be all you need, then you can go through the normal Go processes.

## Execution

I recommend doing this in two different terminal windows/tabs. You need to run this first,
obviously:

    go build

### Consumer

This will wait forever until you give it a `Ctrl + C`:

    ./spikerabbit consume
    
### Producer

    ./spikerabbit produce

## Contributing

Wanna write more things?  I have no real guidlines besides these:

 * Document and comment the hell out of everything
 * You need to make [pull/merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) or I'll ignore you
 * Be patient if I don't see your PR, I get really busy and won't be able to babysit the projects too much (especially these spike repositories)
 * Don't be a douche bag
