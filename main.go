package main

import (
	"github.com/jawher/mow.cli"
	"os"
	"spikerabbit/errors"
	"spikerabbit/topic"
)

const QueueTopic = "spike-topic"
const MessageCount = 2048

func main() {
	// depending on what we're doing, launch the appropriate handling
	app := cli.App("Spike: Go+RabbitMQ", "Simple application for testing Go integration with RabbitMQ")

	app.Command("consume", "Consume any messages that get posted to the queue", func(cmd *cli.Cmd) {
		connection := topic.OpenConnection()
		defer topic.CloseConnection(connection)
		topic.ConsumeMessages(connection, QueueTopic)
	})

	app.Command("produce", "Produce messages on the queue", func(cmd *cli.Cmd) {
		connection := topic.OpenConnection()
		defer topic.CloseConnection(connection)
		topic.SendMessages(connection, QueueTopic, MessageCount)
	})

	// run the application
	err := app.Run(os.Args)
	errors.FailOnError(err, "Unable to launch the application")
}
