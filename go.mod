module spikerabbit

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/jawher/mow.cli v1.1.0
	github.com/streadway/amqp v1.0.0
)
