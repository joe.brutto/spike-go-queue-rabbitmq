package topic

import (
	"github.com/streadway/amqp"
	"log"
	"spikerabbit/errors"
)

// Consumes and prints any messages for the given topic.
func ConsumeMessages(connection *amqp.Connection, topic string) {
	// connect to the channel
	channel := openChannel(connection)
	defer CloseChannel(channel)

	// connect to the queue
	queue := initializeQueue(channel, topic)

	// consume messages
	messages, err := channel.Consume(queue.Name, "", true, false, false, false, nil)
	errors.FailOnError(err, "Unable to initialize consumer")

	forever := make(chan bool)

	go func() {
		for message := range messages {
			log.Printf("Received Message: %s", string(message.Body))
		}
	}()

	<-forever
}
