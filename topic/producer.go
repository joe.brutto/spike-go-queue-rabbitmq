package topic

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/streadway/amqp"
	"spikerabbit/errors"
)

// Sends a bunch of messages to the queue.
func SendMessages(connection *amqp.Connection, topic string, messageCount int) {
	// open a connection to the channel
	channel := openChannel(connection)
	defer CloseChannel(channel)

	// connect to the queue
	queue := initializeQueue(channel, topic)

	// create a unique ID for the producer
	producerUuid, err := uuid.NewUUID()
	errors.FailOnError(err, "Unable to generate unique Publisher ID")
	producerId := producerUuid.String()

	// publish the messages
	for index := 0; index < messageCount; index++ {
		fmt.Printf("Sending message: %d\n", index + 1)

		amqpMessage := amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(fmt.Sprintf("Producer[%s], Message[%d]", producerId, index + 1)),
		}

		err := channel.Publish("", queue.Name, false, false, amqpMessage)
		if err != nil {
			errors.LogError(err, "Unable to publish message to topic")
		}
	}
}
