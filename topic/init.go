package topic

import (
	"fmt"
	"github.com/streadway/amqp"
	"spikerabbit/errors"
)

// Connects to RabbitMQ
func OpenConnection() *amqp.Connection {
	connection, err := amqp.Dial("amqp://spike:spike@localhost:5672/spike")
	errors.FailOnError(err, "Unable to connect to RabbitMQ")
	return connection
}

// Opens a Channel on the queue.
func openChannel(connection *amqp.Connection) *amqp.Channel {
	channel, err := connection.Channel()
	errors.FailOnError(err, "Unable to open channel for topic initialization")
	return channel
}

// Initializes the queue for our testing.
func initializeQueue(channel *amqp.Channel, topic string) amqp.Queue {
	// declare the queue & exit
	fmt.Printf("Declare queue \"%s\"\n", topic)
	queue, err := channel.QueueDeclare(topic, true, false, false, false, nil)
	errors.FailOnError(err, fmt.Sprintf("Unable to declare topic queue named \"%s\"", topic))
	return queue
}
