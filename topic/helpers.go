package topic

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

// "Kind" clean-up for AMQP connections.
func CloseConnection(connection *amqp.Connection) {
	log.Printf("Closing connection")
	err := connection.Close()
	if err != nil {
		fmt.Println(fmt.Errorf("error closing RabbitMQ connection: %s", err))
	}
}

// Closes an open channel and fails on any errors.
func CloseChannel(channel *amqp.Channel) {
	log.Printf("Closing channel connection")
	err := channel.Close()
	if err != nil {
		fmt.Println(fmt.Errorf("error closing RabbitMQ channel: %s", err))
	}
}
